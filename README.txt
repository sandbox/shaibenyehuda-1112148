This module allows you to connect jBart widgets to your drupal database. It is done by defining a view feed.

jBart is a jQuery based toolkit with which you can easily build and customize interactive components within drupal:
	Rich search boxes, 
	Tiles and galleries with interactive filters, 
	Tables, 
	Teasers,
	Master detail widgets,
	and many more.

As a jQuery based library, jBart components can be put in any page or any block, or any place you can embed html/javascript.

