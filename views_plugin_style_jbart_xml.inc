<?php
/**
 * @file
 * The jBart plugin Style. 'Renders' xml with fields as attributes.
 */
 
class views_plugin_style_jbart_xml extends views_plugin_style {
 
  function xml_escape($str) {
    $str = str_replace('&', '&amp;', $str);
    $str = str_replace('"', '&quot;', $str);
    $str = str_replace("'", '&apos;', $str);
    $str = str_replace('<', '&lt;', $str);
    $str = str_replace('>', '&gt;', $str);
    $str = str_replace('>', '&gt;', $str);
    $str = str_replace(array("\r", "\r\n", "\n"), '', $str);
    
    return $str;
  }

  function cleanFieldId($field_id) {
    if (substr($field_id, 0, 6) == 'field_') 
      $field_id = substr($field_id, 6);
    if (substr($field_id, strlen($field_id)-6) == '_value') 
      $field_id = substr($field_id, 0, strlen($field_id)-6);
    return $this->xml_escape($field_id);
  }
  
  function extractString($string, $start, $end) {
    $string = " " . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return "";
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
  }
  
  function render() {
     drupal_set_header('Content-Type: application/xml; charset=utf-8');
   $fields = $this->view->field;
   $items = '<items>';
     foreach ($this->view->result as $row) {
       $items .= '<item ';
     foreach ($fields as $fld => $field) {
      $row_value = $field->render($row);
      //ubcart fixing
      if ($this->cleanFieldId($fld) == "sell_price") {
        $ini = strpos($string, '<span class="uc-price-product');
        if ($ini == 0) {
          $tmp = $this->extractString($row_value, 'price">', '<');
          $tmp = str_replace('$', '', $tmp);
          $row_value = str_replace(',' , '', $tmp);
        }
      }
      
      if ($this->cleanFieldId($fld) || "buyitnowbutton" or $this->cleanFieldId($fld) == "addtocartlink") {
        $ini = strpos($string, 'action="');
        if ($ini == 0) 
          $row_value = str_replace('action="', 'action="http://' . $_SERVER['HTTP_HOST'], $row_value);
      }
      
      if ($this->cleanFieldId($fld) == "image_cache_fid")
        $fld = str_replace('image_cache_fid', 'image', $fld);
      
      //end ubcart fixing
      $items .=  $this->cleanFieldId($fld) . '="' . $this->xml_escape($row_value) . '" ';
     }
     $items .= ' />';
     }
   $flds = '';
   
   $items .= '</items>';
   return '<?xml version="1.0" encoding="utf-8" ?>' . $items;
   }
   
  function option_definition() {
    $options = parent::option_definition();
    //$options['Widget'] = array('default' => 'Exhibit');

    return $options;
  }

  function options_form(&$form, &$form_state) {
  }
  
  function aa_dump_field_arr($arr) {
     $ret = '';
     foreach ($arr as $fld => $field)
       $ret .= $fld;
   return $ret;
  }  
  function aa_dump_arr($arr) {
     $ret = '';
     foreach ($arr as $item)
       $ret .= $this->aa_dump_atts($item);
   return $ret;
  }  
  function aa_dump_atts($obj) {
     $ret = '';
     foreach ($obj as $key => $val)
       $ret .= $key . ',';
   return $ret;
  }
}