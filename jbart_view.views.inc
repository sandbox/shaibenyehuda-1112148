<?php

function jbart_view_views_plugins() {
  return array(
    'style' => array( 
     'jbart_feed' => array(
        'title' => t('jBart - Xml Feed'),
        'help' => t('Outputs xml for jBart'),
        'handler' => 'views_plugin_style_jbart_xml',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'type' => 'feed',
      ),
	  ));
}
?>