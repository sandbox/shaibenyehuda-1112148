<?php
/**
 * @file
 * jbart view - defines the jbart feed and view.
 */
 
function jbart_views_plugins() {
  return array(
    'style' => array( 
     'jbart_feed' => array(
        'title' => t('jBart - Xml Feed <a style="color: darkblue;text-decoration: underline;" target="_blank" href="http://www.artwaresoft.com/#?page=MyItems">(jBart Studio)</a>'),
        'help' => t('Outputs xml for jBart'),
        'handler' => 'views_plugin_style_jbart_xml',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'type' => 'feed',
      ),
  ));
}
